package ru.codeinside.lessons.javacore.exceptionlab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class OperationFactory {
    private final Logger log = LogManager.getLogger(OperationFactory.class);

    public static void main(String[] args) {
        var operationFactory = new OperationFactory();
        operationFactory.parseAndDivide();
        operationFactory.checkLength();
    }

    private void parseAndDivide() {
        log.info("метод parseAndDivide() успешно стартовал.");
        try {
            List<String> source = List.of("2", "5", "0", "10", "10000000000", "-100", "qwerty");
            source.forEach(s -> System.out.println(1000/Integer.parseInt(s)));
        }catch (ArithmeticException | NumberFormatException e){
            log.error("#parseAndDivide(): invalid divisor value or type " + e);
        }
    }

    private void checkLength() {
        try {
            List<String> source = Arrays.asList("car", "table", "", "01", "alphabet", null, "zero");
            source.forEach(s -> System.out.println(s.length()));
        }catch (NullPointerException | IllegalArgumentException e){
            log.error("#checkLength(): invalid argument type " + e);
        }
    }
}
