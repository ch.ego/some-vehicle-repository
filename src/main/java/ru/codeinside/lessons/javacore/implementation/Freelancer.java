package ru.codeinside.lessons.javacore.implementation;

public class Freelancer implements Service{
    private String nickname = "Yuri";
    private String balance;

    @Override
    public String makeService() {
        return "FREELANCER "+ this.nickname;
    }

    @Override
    public String getName() {
        return this.nickname;
    }
}
