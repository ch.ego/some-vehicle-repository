package ru.codeinside.lessons.javacore.implementation;

public class Company implements Service {
    private String inn = "01234567890";
    private String name = "USUAL CORP";
    private String accountNumber;

    public Company(String inn, String name, String accountNumber) {
        this.inn = inn;
        this.name = name;
        this.accountNumber = accountNumber;
    }

    @Override
    public String makeService() {
        return "COMPANY " + this.name;
    }

    @Override
    public String getName() {
        return name + " " + this.inn;
    }

    @Override
    public String getShortName() {
        return this.name;
    }
}