package ru.codeinside.lessons.javacore.inheritance.enums;

import java.util.*;

/**
 * Перечисление описывает возможные цвета автомобилей.
 */
public enum VehicleColor {
    RED("red"),
    BLUE("blue"),
    GREEN("green"),
    MINT("mint"),
    BLACK("black"),
    WHITE("white"),
    UNDEFINED("undefined");

    private final String color;
    private static final List<VehicleColor> colorList = List.of(values());

    VehicleColor(String color) {
        this.color = color;
    }

    /**
     * Метод возвращает объект {@link VehicleColor} на основе переданного названия цвета.
     *
     * @param searchedColor {@link String} название цвета
     * @return {@link VehicleColor}
     */
    public static VehicleColor getColorByName(String searchedColor) {
        return colorList.stream()
                .filter(colorList -> colorList.getColor().equals(searchedColor)).findFirst()
                .orElse(UNDEFINED);
    }

    public String getColor() {
        return color;
    }
}
