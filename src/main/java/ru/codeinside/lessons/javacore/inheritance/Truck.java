package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

public class Truck extends Vehicle {

    boolean isCanvasBack;

    public Truck(int yearOfProduction, VehicleColor vehicleColor, boolean isCanvasBack) {
        super(yearOfProduction, vehicleColor);
        this.isCanvasBack = isCanvasBack;
    }

    public Truck(int yearOfProduction, String vehicleColor, boolean isCanvasBack) {
        super(yearOfProduction, vehicleColor);
        this.isCanvasBack = isCanvasBack;
    }

    public Truck(String vin, int yearOfProduction, String color, boolean isCanvasBack) {
        super(vin, yearOfProduction, color);
        this.isCanvasBack = isCanvasBack;
    }

    boolean isCanvasBack(){
        return this.isCanvasBack;
    }

    @Override
    public String getVehicleInfo() {
        return "TRUCK:\nVIN: " + vin +
                "\nproduction year: " + yearOfProduction +
                "\ncolor: " + vehicleColor.getColor() +
                "\ncanvas back: " + isCanvasBack;
    }
}
