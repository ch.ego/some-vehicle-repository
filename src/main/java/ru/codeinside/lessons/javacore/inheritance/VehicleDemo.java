package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

/**
 * Класс, демонстрирующий работу лабы про транспорт
 * @author Ch.Ego
 */
public class VehicleDemo {
    public static void main(String[] args) {
        Car car = new Car(Vehicle.generateVin(),2001,VehicleColor.BLACK,true, true);
        Truck truck = new Truck(Vehicle.generateVin(), 1995,"RED", false);
        System.out.println(car.getVehicleInfo());
        System.out.println(truck.getVehicleInfo());
    }
}
