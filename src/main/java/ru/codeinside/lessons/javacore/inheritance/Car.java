package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

public class Car extends Vehicle {
    private boolean isSportCar;
    private boolean isElectroCar;

    public Car(String vin, int yearOfProduction, VehicleColor vehicleColor, boolean isSportCar, boolean isElectroCar) {
        super(vin, yearOfProduction, vehicleColor);
        this.isSportCar = isSportCar;
        this.isElectroCar = isElectroCar;
    }

    @Override
    public String getVehicleInfo() {
        return "CAR:\nVIN: " + vin +
                "\nproduction year: " + yearOfProduction +
                "\ncolor: " + vehicleColor.getColor() +
                "\nsport car: " + isSportCar +
                "\nelectro car: " + isSportCar;
    }
}
