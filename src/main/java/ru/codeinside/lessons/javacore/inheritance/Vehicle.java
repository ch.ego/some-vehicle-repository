package ru.codeinside.lessons.javacore.inheritance;


import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public abstract class Vehicle {

    String vin;
    public int yearOfProduction;
    public VehicleColor vehicleColor;

    public Vehicle(String vin, int yearOfProduction, VehicleColor vehicleColor) {
        this.vin = vin;
        this.yearOfProduction = yearOfProduction;
        this.vehicleColor = vehicleColor;
    }

    public Vehicle(int yearOfProduction, VehicleColor vehicleColor) {
        this.vin = generateVin();
        this.yearOfProduction = yearOfProduction;
        this.vehicleColor = vehicleColor;
    }

    public Vehicle(int yearOfProduction, String vehicleColor) {
        this.vin = generateVin();
        this.yearOfProduction = yearOfProduction;
        this.vehicleColor = VehicleColor.getColorByName(vehicleColor);
    }

    public Vehicle(String vin, int yearOfProduction, String color) {
        this.vin = vin;
        this.yearOfProduction = yearOfProduction;
        this.vehicleColor = VehicleColor.getColorByName(color);
    }

    public VehicleColor getColor() {
        return vehicleColor;
    }

    /**
     * @return Информация о машине
     */
    public abstract String getVehicleInfo();

    /**
     * Метод генерирует псевдо-случайный идентификационный номер транспортного средства.
     * Данный номер используется в целях обучения и не соответствует ISO 3779-1983 и ISO 3780.
     * Переделывать или вносить изменения в этот метод не требуется.
     *
     * @return псевдо-уникальный VIN
     */
    static String generateVin() {
        var source = List.of("ABCDEFGHJKLMNPQRSTUVWXYZ1234567890".split(""));
        var random = new Random();
        return random.ints(17, 0, source.size())
                .boxed()
                .map(source::get)
                .collect(Collectors.joining());
    }

}
